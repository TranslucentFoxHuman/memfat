/*
 * mainwindow.cpp
 * Copyright (C) 2021 半透明狐人間
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.h"

char *chardata;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    int fatsize = 0;
    ui->StatusLabel->setText(QString::fromStdString("Getting fat..."));
    if ("100M" == ui->comboBox->currentText().toStdString()) {
        fatsize = 1024*1024*100;
    } else if ("500M" == ui->comboBox->currentText().toStdString()) {
        fatsize = 1024*1024*500;
    } else {
        fatsize = 1024*1024*1024;
    }
            chardata = new char [fatsize];
            for (int i = 0;i <= fatsize;i++) {
                    chardata[i] = 1;
            }
    ui->StatusLabel->setText(QString::fromStdString("Got fat!!"));
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog *abtdiag = new AboutDialog(this);
        abtdiag->show();
}
