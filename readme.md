# MemFat  
Programs that intentionally consume large amounts of memory.  

**What's this?**   
This is a programs that intentionally consume large amounts of memory.  
You can choose how much memory you want to consume from 100MB, 500MB, or 1GB, and "Get fat!!" button to consume the specified amount of memory.  

**What can we use for?**  
This program can be used for a memory cleaner for Android.  
When memory is consumed to some extent, Android automatically kills unused tasks to free up memory. By taking advantage of this characteristic, you can free up memory on Android by making it unnecessarily consumed.

**Downloads**
You can download the binary build for Android below.
 
ARM : [net.tlfoxhuman.memfat-arm.apk](https://gitlab.com/TranslucentFoxHuman/memfat/-/raw/master/Release/Android/net.tlfoxhuman.memfat-arm.apk)  
ARM64 : [net.tlfoxhuman.memfat-arm64.apk](https://gitlab.com/TranslucentFoxHuman/memfat/-/raw/master/Release/Android/net.tlfoxhuman.memfat-arm64.apk)  
x86 : [net.tlfoxhuman.memfat-x86.apk](https://gitlab.com/TranslucentFoxHuman/memfat/-/raw/master/Release/Android/net.tlfoxhuman.memfat-x86.apk)  
x86_64 : [net.tlfoxhuman.memfat-x86_64.apk](https://gitlab.com/TranslucentFoxHuman/memfat/-/raw/master/Release/Android/net.tlfoxhuman.memfat-x86_64.apk)
 
You're not sure?  
You can download a multi-arch version for any environment (but the file size is very large).  
multi-abi : [net.tlfoxhuman.memfat-multi_abi.apk](https://gitlab.com/TranslucentFoxHuman/memfat/-/raw/master/Release/Android/net.tlfoxhuman.memfat-multi_abi.apk)

**License**  
    Copyright (C) 2021 半透明狐人間  
 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program.  If not, see  [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/) .
 
**How can we contact to the auther?**  
You can contact me using the contact information below.  
Please note that I can only understand Japanese directly, and for other languages, Google Translate or DeepL translation is often used.  
 
GitLab : [https://gitlab.com/TranslucentFoxHuman/](https://gitlab.com/TranslucentFoxHuman/)  
Mastodon : [@tlfoxhuman@kemonodon.club](https://kemonodon.club/tlfoxhuman)  
Twitter : [@TlFoxHuman](https://twitter.com/TlFoxHuman) (Note : If you are not following my Twitter account, you will not be notified even if a reply is sent from you. Therefore, please contact me via direct message.)  
E-mail : [softwares14@tlfoxhuman.net](mailto:softwares14@tlfoxhuman.net)
